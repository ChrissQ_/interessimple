
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interes Simple</title>
    </head>
    <body>
        <h1>Resultado de los datos ingresados</h1>
        <%
            String a = (String) request.getAttribute("Capital");
            String tz = (String) request.getParameter("Tasa");
            String t = (String) request.getParameter("Tiempo");
            float interes = (float) request.getAttribute("Interes");
        %>
        <p>Para el monto: $<%=a%> </p>
        <p>El interes simple es de: $<%=interes%></p>
    </body>
</html>
